package com.hmis.server.init.config;

import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//todo::remove redundant class
//@Configuration
//@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry corsRegistry) {
        corsRegistry.addMapping("/**")
                    .allowedOrigins( "**" )
                    .allowedMethods( "*" )
                    .maxAge( 3600L )
                    .allowedHeaders( "*" )
                    .exposedHeaders( "Authorization" )
                    .allowCredentials( true );
    }
}
